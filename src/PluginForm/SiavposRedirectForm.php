<?php

declare(strict_types = 1);

/**
 * @file
 * Contains the business logic of the payment gateway plugin. Its entry point is
 * the `buildConfigurationForm()` method.
 */

namespace Drupal\commerce_siavpos\PluginForm;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Drupal\commerce_siavpos\FailureHandler;
use Drupal\commerce_siavpos\RequestHandlerInterface;
use Drupal\commerce_siavpos\ResponseHandlerInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Starts payment with the checkout form pane.
 *
 * @package Drupal\commerce_siavpos\PluginForm
 */
class SiavposRedirectForm extends BasePaymentOffsiteForm implements ContainerInjectionInterface {

  use FailureHandler;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The request handler service.
   *
   * @var \Drupal\commerce_siavpos\RequestHandlerInterface
   */
  protected $requestHandler;

  /**
   * The response handler service.
   *
   * @var \Drupal\commerce_siavpos\ResponseHandlerInterface
   */
  protected $responseHandler;

  /**
   * Constructs a new 'SiavposRedirectForm' object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory service.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    MessengerInterface $messenger,
    LoggerInterface $logger,
    RequestHandlerInterface $request_handler,
    ResponseHandlerInterface $response_handler
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->messenger = $messenger;
    $this->logger = $logger;
    $this->requestHandler = $request_handler;
    $this->responseHandler = $response_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('messenger'),
      $container->get('logger.channel.commerce_siavpos'),
      $container->get('commerce_siavpos.request_handler'),
      $container->get('commerce_siavpos.response_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment_entity */
    $payment_entity = $this->entity;

    /** @var \Drupal\commerce_order\Entity\OrderInterface $order_entity */
    $order_entity = $payment_entity->getOrder();

    /** @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $payment_entity->getPaymentGateway()->getPlugin();

    $post_data = [
      'command' => 'v',
    ];

    /** @var string */
    $post_data['amount'] = $payment_entity->getAmount()->getNumber() . '00';

    $plugin_config = $payment_gateway_plugin->getConfiguration();

    if (!isset($plugin_config['currency'])) {
      return $this->_failureHandler('ISO code of accepted currency is not properly configured.');
    }

    $currency_iso = $this->_isPaymentCurrencySupported($payment_entity, $plugin_config['currency']);

    if (!$currency_iso) {
      $message = sprintf('The "%s" payment gateway does not support the currency "%s" used for this payment.',
        $payment_gateway_plugin->getLabel(),
        $payment_entity->getAmount()->getCurrencyCode());
      return $this->_failureHandler($message);
    }

    /** @var string */
    $post_data['currency'] = $currency_iso;

    /** @var string */
    $post_data['client_ip_addr'] = $order_entity->getIpAddress();

    // @todo in the future we may want to pass over some more useful information
    // here towards SIAVPOS. Keep in mind that `$order_entity->getOrderNumber()`
    // returns NULL at this phase of the checkout process.
    /** @var string */
    $post_data['description'] = urlencode($this->t('Cart ID: #@order_id', ['@order_id' => $order_entity->id()])->render());

    if ($order_entity->language() !== $payment_entity->language()) {
      $this->logger->notice($this->t('Language definition mismatch detected between order and its related payment.'));
    }

    $payment_langcode = $payment_entity->language()->getId();
    $payment_langcode = ($payment_langcode !== 'und') ?: 'en';
    $post_data['language'] = $payment_langcode;

    if (!isset($plugin_config['ecomm_server_url'])) {
      return $this->_failureHandler('URL of external payment gateway service server is not properly configured.');
    }

    $response = $this->requestHandler->sendRequest($post_data);

    if (!$response) {
      return $this->_failureHandler('Call of the external payment gateway service returned with an unspecified error.');
    }

    $response_content_deserialized = $this->responseHandler->handleResponse($response, $post_data['command']);
    $payment_entity->setRemoteId($response_content_deserialized['TRANSACTION_ID'])->save();
    $url = Url::fromUri($plugin_config['ecomm_client_url'], [
      'query' => [
        'trans_id' => $payment_entity->getRemoteId(),
        'language' => $payment_langcode],
      'absolute' => TRUE,
      'https' => TRUE
    ]);

    return $this->buildRedirectForm($form, $form_state, $url->toString(), [], 'POST');
  }

  /**
   * Checks whether this payment gateway is capable to process the currency of
   * the given Drupal Commerce payment object or not?
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   * @param string $accepted_currency_iso
   *
   * @return bool|string
   */
  protected function _isPaymentCurrencySupported(PaymentInterface $payment, string $accepted_currency_iso) {

    // Three-letter acronym
    $payment_currency_tla = $payment->getAmount()->getCurrencyCode();

    $existing_currencies_filtered = $this->entityTypeManager
      ->getStorage('commerce_currency')
      ->loadByProperties(['currencyCode' => $payment_currency_tla]);
    // We can rest assured that only one currency entity exists within the
    // system with a given currency code string. Check happens here:
    // Drupal\commerce_price\Form\CurrencyForm::validateCurrencyCode() line #111
    $payment_currency_entity = array_shift($existing_currencies_filtered);

    $payment_currency_iso = $payment_currency_entity->getNumericCode();

    if ($payment_currency_iso === $accepted_currency_iso) {
      return $payment_currency_iso;
    } else {
      return FALSE;
    }
  }
}
