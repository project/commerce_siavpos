<?php

declare(strict_types = 1);

namespace Drupal\commerce_siavpos\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Defines the “business day closure” custom content entity type.
 *
 * @ingroup commerce_siavpos
 *
 * @ContentEntityType(
 *   id = "business_day_closure",
 *   label = @Translation("Business day closure"),
 *   label_collection = @Translation("Business day closures"),
 *   label_singular = @Translation("business day closure"),
 *   label_plural = @Translation("business day closures"),
 *   label_count = @PluralTranslation(
 *     singular = "@count business day closure",
 *     plural = "@count business day closures",
 *   ),
 *   handlers = {
 *     "storage" = "Drupal\Core\Entity\Sql\SqlContentEntityStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *   },
 *   list_cache_contexts = {
 *     "user"
 *   },
 *   base_table = "business_day_closure",
 *   admin_permission = "administer commerce_payment",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *   }
 * )
 */
class BusinessDayClosure extends ContentEntityBase implements BusinessDayClosureInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    /** @var \Drupal\Core\Field\BaseFieldDefinition[] $fields */
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['timestamp'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Received on'))
      ->setDescription(t('Date and time of receiving a response to business day closure request, as a Unix timestamp.'))
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'timestamp_ago',
        'weight' => 1,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['result_value'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Result value'))
      ->setTranslatable(FALSE)
      ->setDisplayConfigurable('view', FALSE)
      ->setDisplayConfigurable('form', FALSE);

    $fields['result_code'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Result code'))
      ->setTranslatable(FALSE)
      ->setDisplayConfigurable('view', FALSE)
      ->setDisplayConfigurable('form', FALSE);

    foreach (static::fieldNameMapping() as $remote_field_name => $local_field_name) {

      // First, let's skip these two fields as they are already added above.
      if (preg_match('/^result/', $remote_field_name)) {
        continue;
      }

      // Then generate a human label from machine name.
      $field_label = str_replace('_', ' ', $local_field_name);
      $field_label = preg_replace('/^(\S+)\s(\S+)/i', 'Total $1 of $2', $field_label);

      // Finally build up the field definition list.
      $fields[$local_field_name] = BaseFieldDefinition::create('integer')
        ->setLabel(t($field_label))
        ->setTranslatable(FALSE)
        ->setDisplayConfigurable('view', FALSE)
        ->setDisplayConfigurable('form', FALSE);
    }

    return $fields;
  }

  /**
   * A centrally stored switchboard to translate 3rd-party field names to more meaningful ones.
   *
   * @return string[]
   */
  public static function fieldNameMapping() : array {
    return [
        'RESULT' => 'result_value',
        'RESULT_CODE' => 'result_code',
        'FLD_074' => 'number_credit_transactions',
        'FLD_075' => 'number_credit_reversals',
        'FLD_076' => 'number_debit_transactions',
        'FLD_077' => 'number_debit_reversals',
        'FLD_086' => 'amount_credit_transactions',
        'FLD_087' => 'amount_credit_reversals',
        'FLD_088' => 'amount_debit_transactions',
        'FLD_089' => 'amount_debit_reversals',
      ];
  }

  /**
   * {@inheritdoc}
   */
  public function getChangedTime() {
    // @todo: Implement getChangedTime() method.
  }

  /**
   * {@inheritdoc}
   */
  public function setChangedTime($timestamp) {
    // @todo: Implement setChangedTime() method.
  }

  /**
   * {@inheritdoc}
   */
  public function getChangedTimeAcrossTranslations() {
    // @todo: Implement getChangedTimeAcrossTranslations() method.
  }

  /**
   * {@inheritdoc}
   */
  public function getPaymentGateway() {
    // @todo: Implement getPaymentGateway() method.
  }

  /**
   * {@inheritdoc}
   */
  public function getPaymentGatewayId() {
    // @todo: Implement getPaymentGatewayId() method.
  }

  /**
   * {@inheritdoc}
   */
  public function getDateOfBusinessDay() {
    $field_value = $this->get('timestamp')->getValue();
    return (floor($field_value[0]['value']/86400)*86400)-1;
  }

}
