<?php

declare(strict_types = 1);

/**
 * @file
 * Contains the payment gateway plugin for Drupal Commerce. Most of the business
 * logic happens in the class annotated under the `offsite-payment` key.
 */

namespace Drupal\commerce_siavpos\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines the plugin type of the "SIA Virtual POS" payment gateway.
 *
 * @\Drupal\commerce_payment\Annotation\CommercePaymentGateway(
 *   id = "siavpos",
 *   label = @Translation("SIA Virtual POS"),
 *   display_label = @Translation("SIA Virtual POS"),
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_siavpos\PluginForm\SiavposRedirectForm",
 *   },
 *   credit_card_types = {"mastercard", "visa"},
 * )
 */
class Siavpos extends OffsitePaymentGatewayBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
        'ecomm_server_url' => 'https://vpos.te.sia.eu:8443/ecomm/MerchantHandler',
        'ecomm_client_url' => 'https://vpos.te.sia.eu/ecomm/ClientHandler',
        'cert_url' => '/var/www/html/domain/certs/1234567keystore.pem',
        'currency' => '348'
      ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {

    $form = parent::buildConfigurationForm($form, $form_state);

    $form['ecomm_server_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('ECOMM server URL'),
      '#description' => $this->t('Electronic Commerce System server URL.'),
      '#default_value' => $this->configuration['ecomm_server_url'],
      '#required' => TRUE,
    ];

    $form['ecomm_client_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('ECOMM client URL'),
      '#description' => $this->t('Electronic Commerce System client URL.'),
      '#default_value' => $this->configuration['ecomm_client_url'],
      '#required' => TRUE,
    ];

    $form['cert_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Certification URL'),
      '#description' => $this->t('URL of the certification.'),
      '#default_value' => $this->configuration['cert_url'],
      '#required' => TRUE,
    ];

    $form['cert_pass'] = [
      '#type' => 'password',
      '#title' => $this->t('Keystore file password'),
      '#description' => $this->t('Password of the keystore file.'),
      '#default_value' => $this->configuration['cert_pass'],
      '#required' => TRUE,
    ];

    $form['currency'] = [
      '#type' => 'select',
      '#options' => [978 => 'EUR', 840 => 'USD', 941 => 'RSD', 703 => 'SKK',
        440 => 'LTL', 233 => 'EEK', 643 => 'RUB', 891 => 'YUM', 348 => 'HUF'],
      '#title' => $this->t('Currency'),
      '#description' => $this->t('Currency used for transactions.'),
      '#default_value' => $this->configuration['currency'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $values = $form_state->getValue($form['#parents']);

    foreach (['ecomm_server_url', 'ecomm_client_url', 'cert_url', 'cert_pass', 'currency'] as $setting_name) {
      $this->configuration[$setting_name] = $values[$setting_name];
    }
  }

}
