<?php

declare(strict_types = 1);

namespace Drupal\commerce_siavpos;

interface RequestHandlerInterface {

  /**
   * @param array $post_data
   *   Associative array of the following values necessary to be set for an external API call:
   *     - 'command':
   *       - 'v' for the initial registration of a newly created transaction
   *       - 'c' for verifying the results of an already existing transaction
   *       - 'b' for initiating a business day closure
   *
   *   When 'command' is set to 'v' then array items are expected to be:
   *     - 'amount'
   *     - 'currency'
   *     - 'client_ip_addr'
   *     - 'description'
   *     - 'language'
   *
   *   When 'command' is set to 'c' then array items are expected to be:
   *     - 'trans_id'
   *     - 'client_ip_addr'
   *
   *   When 'command' is set to 'b' then no further array items are needed.
   *
   * @return string|false
   *   The response received from the call or false if any error occurred.
   *
   * @see \curl_exec
   */
  public function sendRequest(array $post_data);

}
